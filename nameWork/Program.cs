﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace _30_12
{
    class Program
    {
        
        public class Person
        {
            public string name;
            public int nameWork;
            public int plata;
            public bool post;
        }

        public static void ReadPeople(List<Person> persons)
        {
            string[] text = File.ReadAllLines(@"../../../person.txt");

            string[][] text1people = new string[text.Length][];
            for (int a = 0; a < text.Length; a++)
            {
                text1people[a] = text[a].Split(';');
            }
            Convert2(text1people, persons);
        }

        public static void Convert2(string[][] text1people, List<Person> persons)
        {
            for (int a = 0; a < text1people.Length; a++)
            {
                Person newPerson = new Person();
                newPerson.name = text1people[a][0];
                newPerson.nameWork = Convert.ToInt32(text1people[a][1].Substring(4));
                newPerson.plata = Convert.ToInt32(text1people[a][2]);
                if(text1people[a].Length == 4)
				{
                    newPerson.post = Convert.ToBoolean(text1people[a][3]);
                }
                persons.Add(newPerson);
            }
        }

         //найдите отдел с самым высокооплачиваемым руководителем.
        public static int GetMaxPlata(List<Person> persons)
		{
            int maxplata = 0;
            int index = 0;
            foreach(var p in persons)
			{
                if(p.post == true && p.plata > maxplata)
				{
                        maxplata = p.plata;
                        index = p.nameWork;
				}
			}
            return index;
        }

        public static void GetSrPlata (List<Person> persons)
        {
            int max = persons.Max(p => p.nameWork);
            for  ( int i = 1; i <= max; i++)
            {
                int count = 0;
                int summa = 0;
                foreach ( var z in persons)
                {
                    if (z.nameWork == i && z.post == false)
                    {
                        count++;
                        summa += z.plata;
                    }


                }

                double sr = summa / count; 

            }



        }
        
        static void Main(string[] args)
        {
            List<Person> persons = new List<Person>();
            ReadPeople(persons);
            int res = GetMaxPlata(persons);
            Console.WriteLine(res);
        }
    }
}
