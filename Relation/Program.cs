﻿using System;
using System.Collections.Generic;
using System.IO;

namespace zach_1
{
    public enum relationship
    {
        spouse,
        parent,
        sibling
    }

    public struct relat
    {
        public int idfirst;
        public int idsecond;
        public relationship typerel;
    }

    public class Person
    {
        public int id;
        public string lastName;
        public string firstName;
        public DateTime birthDate;
        public List<relat> real;
    }

    class Program
	{
        private const int MaxDateLength = 10;
        private const int MinDateLength = 4;
        static List<Person> persons = new List<Person>();

        static void PeopleToPerson(string[][] text1people, List<relat> reals)
        {
            for (int a = 0; a < text1people.Length; a++)
            {
                if (text1people[a] != null)
                {
                    var person = new Person();
                    person.id = Convert.ToInt32(text1people[a][0]);
                    person.real = new List<relat>();
                    person.lastName = text1people[a][1];
                    person.firstName = text1people[a][2];
                    person.birthDate = stringToDateTime(text1people[a][3]);
                    foreach (var r in reals)
                    {
                        if(r.idfirst == person.id)
                        {
                            person.real.Add(r);
                        }
                    }
                    
                    persons.Add(person);
                }
            } 
        }

        static void ReadPeople()
        {
            string[] text1 = File.ReadAllLines(@"../../../person.txt");

            string[][] text1people = new string[text1.Length][];
            int lastStr = 0;
            for (int a = 0; a < text1.Length; a++)
            {
                if(string.IsNullOrEmpty(text1[a]))
				{
                    lastStr = a;
                    break;
				}
				if (a !=0)
				{
                    text1people[a-1] = text1[a].Split(';');
				}
            }

            List<relat> reals = new List<relat>();
            for (int a = lastStr+1; a < text1.Length; a++)
            {
                int indexCloseBrek = text1[a].IndexOf('>');
                int idfirst = Convert.ToInt32(text1[a].Substring(0,1));
                int idsecond = Convert.ToInt32(text1[a].Substring(indexCloseBrek+1,1));
                string relation = text1[a].Substring(indexCloseBrek+3);
                relationship type = relationship.spouse;
                if (relation == "spouse")
				{
                    type = relationship.spouse;
                }
                else if (relation == "parent")
                {
                    type = relationship.parent;
                }
                else if (relation == "sibling")
                {
                    type = relationship.sibling;
                }
                var real = new relat();
                real.idfirst = idfirst;
                real.idsecond = idfirst;
                real.typerel = type;
                reals.Add(real);
            }
            
            PeopleToPerson(text1people, reals);
        }

        public static DateTime stringToDateTime(string date)
        {
            try
            {
                if (date.Length <= MaxDateLength)
                {
                    var dateTime = new DateTime();
                    if (date.Length == MinDateLength)
                    {
                        dateTime = new DateTime(int.Parse(date), 1, 1);
                    }
                    else
                    {
                        dateTime = DateTime.Parse(date);
                    }
                    return dateTime;
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

            return new DateTime();
        }
        static void Main(string[] args)
		{
            ReadPeople();



        }
	}
}
