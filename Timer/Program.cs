﻿using System;
using System.Collections.Generic;
using System.Timers;

namespace ConsoleApp1
{
    internal class Program
    {
        public class test
        {
            public string Name;
            public DateTime start;
            public DateTime end;

            public test(string name, DateTime start1, DateTime end2)
            {
                Name = name;
                start = start1;
                end = end2;
            }

        }

        public static List<test> testList = new List<test>();
        private static Timer aTimer;

        public static void Main()
        {
            testList.Add(new test("1object", DateTime.Now.AddSeconds(1), DateTime.Now.AddSeconds(2)));
            testList.Add(new test("2object", DateTime.Now.AddSeconds(2), DateTime.Now.AddSeconds(5)));
            testList.Add(new test("3object", DateTime.Now.AddSeconds(10), DateTime.Now.AddSeconds(5)));
            testList.Add(new test("4object", DateTime.Now.AddSeconds(15), DateTime.Now.AddSeconds(5)));

            SetTimer();

            Console.WriteLine("\nPress the Enter key to exit the application...\n");
            Console.WriteLine("The application started at {0:HH:mm:ss.fff}", DateTime.Now);
            Console.ReadLine();
            aTimer.Stop();
            aTimer.Dispose();

            Console.WriteLine("Terminating the application...");
        }

        private static void SetTimer()
        {
            // Create a timer with a one second interval.
            aTimer = new System.Timers.Timer(1000);
            // Hook up the Elapsed event for the timer. 
            aTimer.Elapsed += OnTimedEvent;
            aTimer.AutoReset = true;
            aTimer.Enabled = true;
        }

        private static void OnTimedEvent(Object source, ElapsedEventArgs e)
        {
            foreach(var t in testList)
            {
                if(t.start.Minute == DateTime.Now.Minute && t.start.Second == DateTime.Now.Second)
                {
                    Console.WriteLine("The app time start {0:HH:mm:ss.fff} equal start in object {1} ", DateTime.Now, t.Name);
                }
            }
        }
    }
}
